<?php

namespace TeamSpace\TaskBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TaskType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('subject', 'text', array(
                    'attr' => array(
                        'class' => 'input-xlarge'
                    )
                ))
            ->add('description', 'textarea', array(
                    'attr' => array(
                        'rows' => '3',
                        'class' => 'input-xlarge'
                    )
                ))
            ->add('status', null, array(
                    'attr' => array(
                        'class' => 'input-xlarge'
                    )
                ))
            ->add('parent', null, array(
                    'property' => 'indentedTitle',
                    'attr' => array(
                        'class' => 'input-xlarge'
                    )
                ))
            ->add('tracker', null, array(
                    'attr' => array(
                        'class' => 'input-xlarge'
                    )
                ))
            ->add('startDate', 'genemu_jquerydate', array(
                    'widget' => 'single_text',
                    'required' => false
               ))
            ->add('dueDate', 'genemu_jquerydate', array(
                    'widget' => 'single_text',
                    'required' => false
               ))
            ->add('assigned_to', 'genemu_jqueryselect2_entity', array(
                    'class' => 'TeamSpace:User',
                    'property' => 'indentedTitle',
                    'attr' => array(
                        'class' => 'input-xlarge'
                    )
                ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'TeamSpace\Entity\Task'
        ));
    }

    public function getName()
    {
        return 'task';
    }
}

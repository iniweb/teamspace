<?php

namespace TeamSpace\ProjectBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use TeamSpace\Entity\Project;
use TeamSpace\ProjectBundle\Form\ProjectType;

/**
 * Class ProjectController
 *
 * @package TeamSpace\ProjectBundle\Controller
 */
class ProjectController extends Controller
{
    /**
     * List of projects
     *
     * @return Response
     */
    public function indexAction()
    {
        $repository = $this->getProjectRepository();

        $projectsQuery = $repository->getActiveProjectsTreeQuery();

        $projects = $repository->buildTree($projectsQuery->getArrayResult(), array('decorate' => false));

        return $this->render(
            'TeamSpaceProjectBundle:Project:index.html.twig',
            array(
                'projects' => $projects
            )
        );
    }

    /**
     * Project output
     *
     * @param Request $request
     * @param int     $identifier Project identifier.
     *
     * @throws NotFoundHttpException
     * @return Response
     */
    public function showAction(Request $request, $identifier)
    {
        $project = $this->getProjectRepository()
            ->findOneBy(
                array(
                    'identifier' => $identifier
                )
            );

        if (!$project) {
            throw $this->createNotFoundException('Unable to find project ' . $identifier);
        }

        $tasksQuery = $this->getDoctrine()
            ->getRepository('TeamSpace:Task')
            ->getQueryTasksByProjectId($project->getId());

        $limit = $this->container->getParameter('team_space_project.tasks.limit', 10);
        $pagination = $this->get('knp_paginator')->paginate($tasksQuery, $request->get('page', 1), $limit);

        return $this->render(
            'TeamSpaceProjectBundle:Project:show.html.twig',
            array(
                'project' => $project,
                'pagination' => $pagination
            )
        );
    }

    /**
     * Create new project.
     *
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function createAction(Request $request)
    {
        $project = new Project();
        $project->setAuthor($this->getUser());

        $form = $this->createForm(new ProjectType(), $project);

        $isValid = $this->processForm($form, $request, $project);

        if($isValid) {
            $this->get('session')->getFlashBag()->add('notice', 'New project was created!');

            $redirectUrl = $this->generateUrl('project_show', array('identifier' => $project->getIdentifier()));

            return $this->redirect($redirectUrl);
        }

        return $this->render(
            'TeamSpaceProjectBundle:Project:create.html.twig',
            array(
                'form' => $form->createView()
            )
        );
    }

    /**
     * Edit exist project.
     *
     * @param int     $identifier Project identifier.
     * @param Request $request
     *
     * @throws NotFoundHttpException
     * @return Response
     */
    public function editAction($identifier, Request $request)
    {
        $project = $this->getProjectRepository()
            ->findOneBy(
                array(
                    'identifier' => $identifier
                )
            );

        if (!$project) {
            throw $this->createNotFoundException('No project found by ' . $identifier);
        }

        $form = $this->createForm(new ProjectType(), $project);

        $isValid = $this->processForm($form, $request, $project);

        if($isValid) {
            $this->get('session')->getFlashBag()->add('notice', 'Your changes were saved!');

            $redirectUrl = $this->generateUrl('project_show', array('identifier' => $project->getIdentifier()));

            return $this->redirect($redirectUrl);
        }

        return $this->render(
            'TeamSpaceProjectBundle:Project:edit.html.twig',
            array(
                'form' => $form->createView(),
                'project' => $project
            )
        );
    }

    /**
     * Process project form, new or edit.
     *
     * @param Form    $form
     * @param Request $request
     * @param Project $project
     *
     * @return bool|RedirectResponse
     */
    private function processForm(Form $form, Request $request, Project $project)
    {
        if ($request->getMethod() == 'POST') {

            $form->handleRequest($request);

            if ($form->isValid()) {

                $em = $this->getDoctrine()->getManager();
                $em->persist($project);
                $em->flush();

                return true;
            }

        }

        return false;
    }

    /**
     * Get project repository
     *
     * @return \TeamSpace\Entity\Repository\ProjectRepository
     */
    private function getProjectRepository()
    {
        return $this->getDoctrine()
            ->getRepository('TeamSpace:Project');
    }
}

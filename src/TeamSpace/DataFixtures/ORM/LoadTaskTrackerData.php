<?php

namespace TeamSpace\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use TeamSpace\Entity\TaskTracker;

class LoadTaskTrackerData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * Load project tasks fixtures.
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $trackers = array(
            array('value' => 'Задача'),
            array('value' => 'Ошибка'),
            array('value' => 'Улучшение'),
            array('value' => 'Поддержка'),
            array('value' => 'Новый проект'),
            array('value' => 'Вопрос'),
            array('value' => 'Оценка')
        );

        foreach($trackers as $trackerData) {

            $tracker = new TaskTracker();
            $tracker->setTitle($trackerData['value']);

            $manager->persist($tracker);
        }

        $manager->flush();
        $manager->clear();
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 4;
    }
}
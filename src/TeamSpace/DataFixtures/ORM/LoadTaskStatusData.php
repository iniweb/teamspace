<?php

namespace TeamSpace\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use TeamSpace\Entity\TaskStatus;

class LoadTaskStatusData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * Load project tasks fixtures.
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $statuses = array(
            array('value' => 'Новый', 'is_default' => true, 'is_closed' => false),
            array('value' => 'На проверку', 'is_default' => false, 'is_closed' => false),
            array('value' => 'Назначен', 'is_default' => false, 'is_closed' => false),
            array('value' => 'В процессе', 'is_default' => false, 'is_closed' => false),
            array('value' => 'Отложен', 'is_default' => false, 'is_closed' => false),
            array('value' => 'Выполнен', 'is_default' => false, 'is_closed' => true),
            array('value' => 'Закрыт', 'is_default' => false, 'is_closed' => true)
        );

        foreach($statuses as $statusData) {

            $status = new TaskStatus();
            $status->setTitle($statusData['value']);
            $status->setIsDefault($statusData['is_default']);
            $status->setIsClosed($statusData['is_closed']);

            $manager->persist($status);
        }

        $manager->flush();
        $manager->clear();
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 3;
    }
}
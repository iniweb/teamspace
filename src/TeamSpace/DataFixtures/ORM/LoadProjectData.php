<?php

namespace TeamSpace\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use Faker;
use TeamSpace\Entity\Project;

class LoadProjectData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create('ru');
        $limit = 12;

        for($i = 1; $i <= $limit; $i++) {

            $name = $faker->sentence;
            $identifier = mb_strtolower(str_replace(' ', '', substr($name, 0, 8)));

            $author = $manager->getRepository('TeamSpace:User')
                ->findOneBy(
                    array(
                        'username' => 'iniweb'
                    )
                );

            $project = new Project();
            $project->setName($name);
            $project->setAuthor($author);
            $project->setIdentifier($identifier);
            $project->setDescription($faker->text(300));
            $project->setStatus(1);
            $project->setIsActive(1);

            $manager->persist($project);
        }

        $manager->flush();
        $manager->clear();

    }

    public function getOrder()
    {
        return 2;
    }
}
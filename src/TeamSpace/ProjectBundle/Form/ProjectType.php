<?php

namespace TeamSpace\ProjectBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class ProjectType
 *
 * @package TeamSpace\ProjectBundle\Form
 */
class ProjectType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', array(
                    'attr' => array(
                        'class' => 'input-xlarge'
                    )
                ))
            ->add('identifier', 'text', array(
                'attr' => array(
                    'class' => 'input-xlarge'
                )
            ))
            ->add('parent', null, array(
                    'property' => 'indentedTitle',
                    'attr' => array(
                        'class' => 'input-xlarge'
                    )
                ))
            ->add('description', 'textarea', array(
                    'attr' => array(
                        'rows' => '3',
                        'class' => 'input-xlarge'
                    )
                ))
            ->add('members', 'genemu_jqueryselect2_entity', array(
                    'class' => 'TeamSpace:ProjectMember',
                    'multiple' => true
                ));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'TeamSpace\Entity\Project',
                'translation_domain' => 'TeamSpaceProjectBundle'
            )
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'project';
    }
}
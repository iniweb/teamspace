<?php

namespace TeamSpace\Entity\Repository;

use Gedmo\Tree\Entity\Repository\NestedTreeRepository;

/**
 * Class TaskRepository
 *
 * @package TeamSpace\Entity\Repository
 */
class TaskRepository extends NestedTreeRepository
{
    /**
     * Get tasks query, filtered by project id.
     *
     * @param int $projectId
     *
     * @return \Doctrine\ORM\Query
     */
    public function getQueryTasksByProjectId($projectId)
    {
        return $this->createQueryBuilder('t')
            ->innerJoin('t.project', 'p')
            ->where('t.project = :projectId')
            ->setParameter('projectId', $projectId)
            ->orderBy('t.created_at', 'DESC')
            ->getQuery();
    }
}
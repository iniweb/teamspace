<?php

namespace TeamSpace\TaskBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use TeamSpace\Entity\Task;
use TeamSpace\TaskBundle\Form\TaskType;

/**
 * Task controller.
 *
 */
class TaskController extends Controller
{
    /**
     * Lists all Task entities.
     *
     */
    public function indexAction($identifier)
    {
        $em = $this->getDoctrine()->getManager();

        $project = $em->getRepository('TeamSpace:Project')
            ->findOneBy(
                array(
                    'identifier' => $identifier
                )
            );

        if (!$project) {
            throw $this->createNotFoundException('Unable to find project ' . $identifier);
        }

        $tasks = $em->getRepository('TeamSpace:Task')
            ->findBy(
                array(
                    'project' => $project->getId()
                )
            );

        return $this->render(
            'TeamSpaceTaskBundle:Task:index.html.twig',
            array(
                'tasks' => $tasks,
                'project' => $project
            )
        );
    }

    /**
     * Creates a new Task entity.
     *
     */
    public function createAction(Request $request, $identifier)
    {
        $em = $this->getDoctrine()->getManager();

        $project = $em->getRepository('TeamSpace:Project')
            ->findOneBy(
                array(
                    'identifier' => $identifier
                )
            );

        if (!$project) {
            throw $this->createNotFoundException('Unable to find project ' . $identifier);
        }

        $task = new Task();
        $task->setProject($project);
        $task->setAuthor($this->getUser());

        $form = $this->createForm(new TaskType(), $task);
        $form->handleRequest($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($task);
            $em->flush();

            $redirectUrl = $this->generateUrl('task_show', array('id' => $task->getId()));
            return $this->redirect($redirectUrl);
        }

        return $this->render('TeamSpaceTaskBundle:Task:new.html.twig', array(
            'form'    => $form->createView(),
            'project' => $project
        ));
    }

    /**
     * Displays a form to create a new Task entity.
     *
     */
    public function newAction($identifier)
    {
        $em = $this->getDoctrine()->getManager();

        $project = $em->getRepository('TeamSpace:Project')
            ->findOneBy(
                array(
                    'identifier' => $identifier
                )
            );

        if (!$project) {
            throw $this->createNotFoundException('Unable to find project ' . $identifier);
        }

        $task = new Task();
        $task->setProject($project);

        $form = $this->createForm(new TaskType(), $task);

        return $this->render('TeamSpaceTaskBundle:Task:new.html.twig',
            array(
                'project' => $project,
                'form' => $form->createView(),
            )
        );
    }

    /**
     * Finds and displays a Task entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $task = $em->getRepository('TeamSpace:Task')->find($id);

        if (!$task) {
            throw $this->createNotFoundException('Unable to find Task.');
        }

        return $this->render('TeamSpaceTaskBundle:Task:show.html.twig',
            array(
                'task' => $task
            )
        );
    }

    /**
     * Displays a form to edit an existing Task entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TeamSpace:Task')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Task entity.');
        }

        $editForm = $this->createForm(new TaskType(), $entity);

        return $this->render('TeamSpaceTaskBundle:Task:edit.html.twig', array(
            'entity' => $entity,
            'form'   => $editForm->createView(),
        ));
    }

    /**
     * Edits an existing Task entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TeamSpace:Task')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Task entity.');
        }

        $editForm = $this->createForm(new TaskType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('task_edit', array('id' => $id)));
        }

        return $this->render('TeamSpaceTaskBundle:Task:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        ));
    }

    /**
     * Deletes a Task entity.
     *
     * @param int     $id
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $task = $em->getRepository('TeamSpace:Task')->find($id);

        if (!$task) {
            throw $this->createNotFoundException('Unable to find Task.');
        }

        $identifier = $task->getProject()->getIdentifier();

        $em->remove($task);
        $em->flush();

        $redirectUrl = $this->generateUrl('project_show',
            array(
                'identifier' => $identifier
            )
        );

        return $this->redirect($redirectUrl);
    }
}

<?php

namespace TeamSpace\Entity\Repository;

use Gedmo\Tree\Entity\Repository\NestedTreeRepository;
use Doctrine\ORM\Query\Expr;

/**
 * Class ProjectRepository
 *
 * @package TeamSpace\Entity\Repository
 */
class ProjectRepository extends NestedTreeRepository
{
    /**
     * Get query for projects tree.
     *
     * @return \Doctrine\ORM\Query
     */
    public function getActiveProjectsTreeQuery()
    {
        return $this->createQueryBuilder('node')
            ->select('node, pm, u, a')
            ->leftJoin('node.author', 'a', Expr\Join::WITH, 'node.author = a.id')
            ->leftJoin('node.members', 'pm', Expr\Join::WITH, 'node.id = pm.project')
            ->leftJoin('pm.user', 'u', Expr\Join::WITH, 'pm.user = u.id')
            ->where('node.is_active = :is_active')
            ->setParameter('is_active', true)
            ->orderBy('node.root, node.lft', 'ASC')
            ->getQuery();
    }
}
<?php

namespace TeamSpace\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="task_journal_detail")
 * @ORM\HasLifecycleCallbacks
 */
class TaskJournalDetail
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $notes;

    /**
     * @ORM\Column(type="text", length=30, nullable=false)
     */
    protected $property;

    /**
     * @ORM\Column(type="text", length=30, nullable=false)
     */
    protected $key;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $old_value;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $value;

    /**
     * @ORM\ManyToOne(targetEntity="\TeamSpace\Entity\TaskJournal")
     * @ORM\JoinColumn(name="task_journal_id", referencedColumnName="id", nullable=false)
     */
    protected $task_journal;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set notes
     *
     * @param string $notes
     * @return TaskJournalDetail
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;
    
        return $this;
    }

    /**
     * Get notes
     *
     * @return string 
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set property
     *
     * @param string $property
     * @return TaskJournalDetail
     */
    public function setProperty($property)
    {
        $this->property = $property;
    
        return $this;
    }

    /**
     * Get property
     *
     * @return string 
     */
    public function getProperty()
    {
        return $this->property;
    }

    /**
     * Set key
     *
     * @param string $key
     * @return TaskJournalDetail
     */
    public function setKey($key)
    {
        $this->key = $key;
    
        return $this;
    }

    /**
     * Get key
     *
     * @return string 
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * Set old_value
     *
     * @param string $oldValue
     * @return TaskJournalDetail
     */
    public function setOldValue($oldValue)
    {
        $this->old_value = $oldValue;
    
        return $this;
    }

    /**
     * Get old_value
     *
     * @return string 
     */
    public function getOldValue()
    {
        return $this->old_value;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return TaskJournalDetail
     */
    public function setValue($value)
    {
        $this->value = $value;
    
        return $this;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set task_journal
     *
     * @param \TeamSpace\Entity\TaskJournal $taskJournal
     * @return TaskJournalDetail
     */
    public function setTaskJournal(\TeamSpace\Entity\TaskJournal $taskJournal)
    {
        $this->task_journal = $taskJournal;
    
        return $this;
    }

    /**
     * Get task_journal
     *
     * @return \TeamSpace\Entity\TaskJournal 
     */
    public function getTaskJournal()
    {
        return $this->task_journal;
    }
}
<?php

namespace TeamSpace\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use Faker;
use TeamSpace\Entity\Task;

class LoadProjectTaskData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * Load project tasks fixtures.
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create('ru');
        $limit = rand(5, 15);

        $projects = $manager->getRepository('TeamSpace:Project')
            ->findAll();

        foreach($projects as $project) {
            for($i = 1; $i <= $limit; $i++) {

                $author = $manager->getRepository('TeamSpace:User')
                    ->findOneBy(
                        array(
                            'username' => 'iniweb'
                        )
                    );

                $status = $this->getTaskStatus($manager);
                $tracker = $this->getTaskTracker($manager);
                $assigned = $this->getAssignedUser($manager);

                $task = new Task();
                $task->setSubject($faker->sentence);
                $task->setAuthor($author);
                $task->setAssignedTo($assigned);
                $task->setProject($project);
                $task->setDescription($faker->text(300));
                $task->setStatus($status);
                $task->setTracker($tracker);
                $task->setIsClose(false);

                $manager->persist($task);
            }
        }

        $manager->flush();
        $manager->clear();

    }

    /**
     * Get random one project.
     *
     * @param ObjectManager $manager
     *
     * @return object
     */
    protected function getProject(ObjectManager $manager)
    {
        $projectsData = $manager
            ->createQuery('SELECT p, RAND() as rand FROM TeamSpace:Project p ORDER BY rand')
            ->setMaxResults(1)
            ->getOneOrNullResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

        if (!is_null($projectsData)) {
            $projectData = current($projectsData);

            return $manager->getRepository('TeamSpace:Project')
                ->find($projectData['id']);
        }
    }

    /**
     * Get random one task status.
     *
     * @param ObjectManager $manager
     *
     * @return object
     */
    protected function getTaskStatus(ObjectManager $manager)
    {
        $statusesData = $manager
            ->createQuery('SELECT ts, RAND() as rand FROM TeamSpace:TaskStatus ts ORDER BY rand')
            ->setMaxResults(1)
            ->getOneOrNullResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

        if (!is_null($statusesData)) {
            $statusData = current($statusesData);

            return $manager->getRepository('TeamSpace:TaskStatus')
                ->find($statusData['id']);
        }
    }

    /**
     * Get random one task tracker.
     *
     * @param ObjectManager $manager
     *
     * @return object
     */
    protected function getTaskTracker(ObjectManager $manager)
    {
        $trackersData = $manager
            ->createQuery('SELECT tt, RAND() as rand FROM TeamSpace:TaskTracker tt ORDER BY rand')
            ->setMaxResults(1)
            ->getOneOrNullResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

        if (!is_null($trackersData)) {
            $trackerData = current($trackersData);

            return $manager->getRepository('TeamSpace:TaskTracker')
                ->find($trackerData['id']);
        }
    }

    /**
     * Get random one user.
     *
     * @param ObjectManager $manager
     *
     * @return object
     */
    protected function getAssignedUser(ObjectManager $manager)
    {
        $usersData = $manager
            ->createQuery("SELECT a, RAND() as rand FROM TeamSpace:User a WHERE a.roles NOT LIKE '%ROLE_SUPER_ADMIN%' ORDER BY rand")
            ->setMaxResults(1)
            ->getOneOrNullResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

        if (!is_null($usersData)) {
            $userData = current($usersData);

            return $manager->getRepository('TeamSpace:User')
                ->find($userData['id']);
        }
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 5;
    }
}
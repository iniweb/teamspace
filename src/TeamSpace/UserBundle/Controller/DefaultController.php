<?php

namespace TeamSpace\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function showAction($name)
    {
        $user = $this->getDoctrine()
            ->getRepository('TeamSpace:User')
            ->findOneBy(
                array(
                    'username' => $name
                )
            );

        if (!$user) {
            throw $this->createNotFoundException('Unable to find user ' . $name);
        }

        return $this->render('UserBundle:Profile:show.html.twig',
            array(
                'user' => $user
            )
        );
    }
}

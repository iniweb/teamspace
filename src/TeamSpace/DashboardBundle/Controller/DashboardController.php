<?php

namespace TeamSpace\DashboardBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DashboardController extends Controller
{
    public function indexAction()
    {
        return $this->render('TeamSpaceDashboardBundle:Dashboard:index.html.twig', array('name' => ''));
    }
}

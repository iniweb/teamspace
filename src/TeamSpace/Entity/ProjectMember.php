<?php

namespace TeamSpace\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="TeamSpace\Entity\Repository\ProjectMemberRepository")
 * @ORM\Table(name="project_member")
 */
class ProjectMember
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="\TeamSpace\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity="\TeamSpace\Entity\Project")
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id", nullable=false)
     */
    protected $project;

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getUser();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \TeamSpace\Entity\User $user
     * @return ProjectMember
     */
    public function setUser(\TeamSpace\Entity\User $user)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \TeamSpace\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set project
     *
     * @param \TeamSpace\Entity\Project $project
     * @return ProjectMember
     */
    public function setProject(\TeamSpace\Entity\Project $project)
    {
        $this->project = $project;
    
        return $this;
    }

    /**
     * Get project
     *
     * @return \TeamSpace\Entity\Project 
     */
    public function getProject()
    {
        return $this->project;
    }
}
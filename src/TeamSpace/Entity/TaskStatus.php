<?php

namespace TeamSpace\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="task_status")
 */
class TaskStatus
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $title;

    /**
     * @ORM\Column(type="smallint", length=1, nullable=false)
     */
    protected $is_default = 0;

    /**
     * @ORM\Column(type="smallint", length=1, nullable=false)
     */
    protected $is_closed = 0;

    /**
     * Get title to string
     *
     * @return string
     */
    public function __toString()
    {
        return ($this->getTitle()) ? $this->getTitle() : '';
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return TaskStatus
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set is_default
     *
     * @param integer $isDefault
     * @return TaskStatus
     */
    public function setIsDefault($isDefault)
    {
        $this->is_default = $isDefault;
    
        return $this;
    }

    /**
     * Get is_default
     *
     * @return integer 
     */
    public function getIsDefault()
    {
        return $this->is_default;
    }

    /**
     * Set is_closed
     *
     * @param integer $isClosed
     * @return TaskStatus
     */
    public function setIsClosed($isClosed)
    {
        $this->is_closed = $isClosed;
    
        return $this;
    }

    /**
     * Get is_closed
     *
     * @return integer 
     */
    public function getIsClosed()
    {
        return $this->is_closed;
    }
}
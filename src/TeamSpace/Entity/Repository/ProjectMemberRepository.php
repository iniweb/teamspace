<?php

namespace TeamSpace\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class ProjectMemberRepository
 *
 * @package TeamSpace\Entity\Repository
 */
class ProjectMemberRepository extends EntityRepository
{
}
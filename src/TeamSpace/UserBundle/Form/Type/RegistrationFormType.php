<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace TeamSpace\UserBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use FOS\UserBundle\Form\Type\RegistrationFormType as BaseType;

class RegistrationFormType extends BaseType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('first_name', 'text', array('label' => 'form.first_name', 'translation_domain' => 'UserBundle'))
            ->add('last_name', 'text', array('label' => 'form.last_name', 'translation_domain' => 'UserBundle'));

        parent::buildForm($builder, $options);
    }

    public function getName()
    {
        return 'team_space_user_registration';
    }
}
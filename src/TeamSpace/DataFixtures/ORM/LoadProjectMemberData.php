<?php

namespace TeamSpace\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use TeamSpace\Entity\ProjectMember;

class LoadProjectMemberData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * Load project members fixtures.
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $projects = $manager->getRepository('TeamSpace:Project')
            ->findAll();

        foreach($projects as $project) {

            $limit = rand(1,5);

            for($i = 1; $i <= $limit; $i++) {
                $member = $this->getUser($manager);

                if($member) {
                    $projectMember = new ProjectMember();
                    $projectMember->setProject($project);
                    $projectMember->setUser($member);

                    $manager->persist($projectMember);
                }
            }

        }

        $manager->flush();
        $manager->clear();
    }

    /**
     * Get random one user.
     *
     * @param ObjectManager $manager
     *
     * @return object
     */
    protected function getUser(ObjectManager $manager)
    {
        $usersData = $manager
            ->createQuery("SELECT u, RAND() as rand FROM TeamSpace:User u WHERE u.roles NOT LIKE '%ROLE_SUPER_ADMIN%' ORDER BY rand")
            ->setMaxResults(1)
            ->getOneOrNullResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

        if (!is_null($usersData)) {
            $userData = current($usersData);

            return $manager->getRepository('TeamSpace:User')
                ->find($userData['id']);
        }
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 6;
    }
}
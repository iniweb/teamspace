<?php

namespace TeamSpace\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use Faker;
use TeamSpace\Entity\User;

class LoadUserData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create('ru');

        $users = array(
            array(
                'email' => 'admin@webskola.lv',
                'username' => 'admin',
                'password' => 'password',
                'first_name' => 'WebSkola',
                'last_name' => 'Admin',
                'roles' => array(
                    'ROLE_SUPER_ADMIN'
                )
            ),
            array(
                'email' => 'a.kasperovich@axioma.lv',
                'username' => 'a.kasperovich',
                'password' => 'password',
                'first_name' => 'Anton',
                'last_name' => 'Kasperovich',
                'roles' => array(
                    'ROLE_USER'
                )
            ),
        );

        $limit = 10;

        for($i = 1; $i <= $limit; $i++) {
            $users[] = array(
                'email' => $faker->safeEmail,
                'password' => 'password',
                'username' => $faker->userName,
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'roles' => array(
                    'ROLE_USER'
                )
            );
        }

        if(!empty($users)) {
            foreach ($users as $userData) {

                $user = new User();
                $user->setEmail($userData['email']);
                $user->setUsername($userData['username']);
                $user->setFirstName($userData['first_name']);
                $user->setLastName($userData['last_name']);
                $user->setPlainPassword($userData['password']);
                $user->setRoles($userData['roles']);
                $user->setEnabled(true);

                $manager->persist($user);
            }

            $manager->flush();
            $manager->clear();
        }

    }

    public function getOrder()
    {
        return 1;
    }
}
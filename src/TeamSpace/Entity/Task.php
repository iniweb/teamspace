<?php

namespace TeamSpace\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @Gedmo\Tree(type="nested")
 * @ORM\Entity(repositoryClass="TeamSpace\Entity\Repository\TaskRepository")
 * @ORM\Table(name="task")
 * @ORM\HasLifecycleCallbacks
 */
class Task
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Assert\NotBlank()
     */
    protected $subject;

    /**
     * @ORM\Column(type="text", nullable=false)
     * @Assert\NotBlank()
     */
    protected $description;

    /**
     * @Gedmo\TreeLeft
     * @ORM\Column(name="lft", type="integer")
     */
    private $lft;

    /**
     * @Gedmo\TreeLevel
     * @ORM\Column(name="lvl", type="integer")
     */
    private $lvl;

    /**
     * @Gedmo\TreeRight
     * @ORM\Column(name="rgt", type="integer")
     */
    private $rgt;

    /**
     * @Gedmo\TreeRoot
     * @ORM\Column(name="root", type="integer", nullable=true)
     */
    private $root;

    /**
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(targetEntity="TeamSpace\Entity\Task", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="TeamSpace\Entity\Task", mappedBy="parent")
     * @ORM\OrderBy({"lft" = "ASC"})
     */
    private $children;

    /**
     * @ORM\ManyToOne(targetEntity="\TeamSpace\Entity\TaskStatus")
     * @ORM\JoinColumn(name="status_id", referencedColumnName="id", nullable=false)
     */
    protected $status;

    /**
     * @ORM\ManyToOne(targetEntity="\TeamSpace\Entity\Project")
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id", nullable=false)
     */
    protected $project;

    /**
     * @ORM\ManyToOne(targetEntity="\TeamSpace\Entity\User")
     * @ORM\JoinColumn(name="author_id", referencedColumnName="id", nullable=false)
     */
    protected $author;

    /**
     * @ORM\ManyToOne(targetEntity="\TeamSpace\Entity\User")
     * @ORM\JoinColumn(name="assigned_to_id", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank()
     */
    protected $assigned_to;

    /**
     * @ORM\ManyToOne(targetEntity="\TeamSpace\Entity\TaskTracker")
     * @ORM\JoinColumn(name="tracker_id", referencedColumnName="id", nullable=false)
     */
    protected $tracker;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $start_date;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $due_date;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    protected $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    protected $updated_at;

    /**
     * @ORM\Column(type="smallint", length=1, nullable=false)
     */
    protected $is_close = 0;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Project
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAt()
    {
        $this->created_at = new \DateTime();
    }

    /**
     * Get created_at
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @ORM\PreUpdate
     * @ORM\PrePersist
     */
    public function setUpdatedAt()
    {
        $this->updated_at = new \DateTime();
    }

    /**
     * Get updated_at
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set subject
     *
     * @param string $subject
     * @return Task
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    
        return $this;
    }

    /**
     * Get subject
     *
     * @return string 
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set assigned_to
     *
     * @param \TeamSpace\Entity\User $assignedTo
     * @return Task
     */
    public function setAssignedTo(\TeamSpace\Entity\User $assignedTo)
    {
        $this->assigned_to = $assignedTo;
    
        return $this;
    }

    /**
     * Get assigned_to
     *
     * @return \TeamSpace\Entity\User
     */
    public function getAssignedTo()
    {
        return $this->assigned_to;
    }

    /**
     * Set author
     *
     * @param \TeamSpace\Entity\User $author
     * @return Task
     */
    public function setAuthor(\TeamSpace\Entity\User $author = null)
    {
        $this->author = $author;
    
        return $this;
    }

    /**
     * Get author
     *
     * @return \TeamSpace\Entity\User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set project
     *
     * @param \TeamSpace\Entity\Project $project
     * @return Task
     */
    public function setProject(\TeamSpace\Entity\Project $project = null)
    {
        $this->project = $project;
    
        return $this;
    }

    /**
     * Get project
     *
     * @return \TeamSpace\Entity\Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set tracker
     *
     * @param \TeamSpace\Entity\TaskTracker $tracker
     * @return Task
     */
    public function setTracker(\TeamSpace\Entity\TaskTracker $tracker)
    {
        $this->tracker = $tracker;
    
        return $this;
    }

    /**
     * Get tracker
     *
     * @return \TeamSpace\Entity\TaskTracker 
     */
    public function getTracker()
    {
        return $this->tracker;
    }

    /**
     * Set start_date
     *
     * @param \DateTime $startDate
     * @return Task
     */
    public function setStartDate($startDate)
    {
        $this->start_date = $startDate;
    
        return $this;
    }

    /**
     * Get start_date
     *
     * @return \DateTime 
     */
    public function getStartDate()
    {
        return $this->start_date;
    }

    /**
     * Set due_date
     *
     * @param \DateTime $dueDate
     * @return Task
     */
    public function setDueDate($dueDate)
    {
        $this->due_date = $dueDate;
    
        return $this;
    }

    /**
     * Get due_date
     *
     * @return \DateTime 
     */
    public function getDueDate()
    {
        return $this->due_date;
    }

    /**
     * Set status
     *
     * @param \TeamSpace\Entity\TaskStatus $status
     * @return Task
     */
    public function setStatus(\TeamSpace\Entity\TaskStatus $status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return \TeamSpace\Entity\TaskStatus 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set is_close
     *
     * @param integer $isClose
     * @return Task
     */
    public function setIsClose($isClose)
    {
        $this->is_close = $isClose;
    
        return $this;
    }

    /**
     * Get is_close
     *
     * @return integer 
     */
    public function getIsClose()
    {
        return $this->is_close;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set parent
     *
     * @param \TeamSpace\Entity\Task $parent
     * @return Task
     */
    public function setParent(\TeamSpace\Entity\Task $parent = null)
    {
        $this->parent = $parent;
    
        return $this;
    }

    /**
     * Get parent
     *
     * @return \TeamSpace\Entity\Task 
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Get name by lvl (for tree like output).
     *
     * @return string
     */
    public function getIndentedTitle()
    {
        return str_repeat('--', $this->lvl) . $this->getSubject();
    }
}